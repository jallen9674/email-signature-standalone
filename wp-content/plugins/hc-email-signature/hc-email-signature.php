<?php
/**
* Plugin Name: Hennessey Email Signature App
* Plugin URI: http://jasonhennessey.com/
* Description: Creates The Backend for the Vue,js Signature App
* Version: 1.0
* Author: Hennessey Consulting
* Author URI: https://www.jasonhennessey.com
*/



/*---------------------------------
Adding CMB2
----------------------------------*/

require_once dirname( __FILE__ ) . '/cmb2/init.php';

function hc_email_update_cmb_meta_box_url( $url ) {
    $url = '/wp-content/plugins/hc-email-signature/cmb2/';
    return $url;
}

add_filter( 'cmb2_meta_box_url', 'hc_email_update_cmb_meta_box_url' );


/*---------------------------------
Declare Signature Thumbnail
----------------------------------*/

add_image_size( 'hc-email-signature-thumbnail', 70, 70, true );


/*---------------------------------
Declare Post Type
----------------------------------*/


function hc_email_signature() {
    // creating (registering) the custom type
    register_post_type( 'hc_email_signature', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
        // let's now add all the options for this post type
        array( 'labels' => array(
            'name' => __( 'Signatures', 'bonestheme' ), /* This is the Title of the Group */
            'singular_name' => __( 'Signature', 'bonestheme' ), /* This is the individual type */
            'all_items' => __( 'All Signatures', 'bonestheme' ), /* the all items menu item */
            'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
            'add_new_item' => __( 'Add New Attorney', 'bonestheme' ), /* Add New Display Title */
            'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
            'edit_item' => __( 'Edit Signature', 'bonestheme' ), /* Edit Display Title */
            'new_item' => __( 'New Attorney', 'bonestheme' ), /* New Display Title */
            'view_item' => __( 'View Attorney', 'bonestheme' ), /* View Display Title */
            'search_items' => __( 'Search Signature', 'bonestheme' ), /* Search Custom Type Title */
            'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */
            'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
            'parent_item_colon' => ''
            ), /* end of arrays */
            'description' => __( 'These are the Signatures', 'bonestheme' ), /* Custom Type Description */
            'public' => false,
            'publicly_queryable' => false,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => true,
            'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon' => '',  /* get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png'*/
            'rewrite'   => array( 'slug' => 'our-team', 'with_front' => false ), /* you can specify its url slug */
            'has_archive' => false, /* you can rename the slug here */
            'capability_type' => 'post',
            'show_in_rest'       => true,
            'hierarchical' => false,
            /* the next one is important, it tells what's enabled in the post editor */
            'supports' => array( 'title', 'revisions', 'thumbnail')
        ) /* end of options */
    ); /* end of register post type */

}

add_action( 'init', 'hc_email_signature');

/*--------------------------------------------
Declaring Icons for CPTs
---------------------------------------------*/

function hc_email_add_menu_icons_styles(){ ?>
    <style>
        #adminmenu .menu-icon-hc_email_signature div.wp-menu-image:before { content: "\f336"; }
    </style>
<?php }
add_action( 'admin_head', 'hc_email_add_menu_icons_styles' );



/*---------------------------------
Add Attorney Metaboxes
----------------------------------*/


add_action( 'cmb2_init', 'hc_email_metaboxes' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function hc_email_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_hc_';

    /**
     * Sample metabox to demonstrate each field type included
     */
    $hc_email_metabox = new_cmb2_box( array(
        'id'            => $prefix . 'email_signature',
        'title'         => __( 'Signature Information', 'cmb2' ),
        'object_types'  => array( 'hc_email_signature'),
        'show_in_rest' => WP_REST_Server::READABLE

    ) );

    $hc_email_metabox->add_field( array(
        'name' => __( 'Name', 'cmb2' ),
        'desc' => __( 'Enter the name of the team member here.', 'cmb2' ),
        'id'   => $prefix . 'email_name',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

    $hc_email_metabox->add_field( array(
        'name' => __( 'Special Title Information', 'cmb2' ),
        'desc' => __( 'Enter the title (ex. Attorney) of the team member here.', 'cmb2' ),
        'id'   => $prefix . 'email_title',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

    $hc_email_metabox->add_field( array(
        'name' => __( 'Email', 'cmb2' ),
        'desc' => __( 'Enter the email address of the team member here.', 'cmb2' ),
        'id'   => $prefix . 'email_email',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

    $hc_email_metabox->add_field( array(
        'name' => __( 'Phone', 'cmb2' ),
        'desc' => __( 'Enter the phone number of the team member here.', 'cmb2' ),
        'id'   => $prefix . 'email_phone',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

}


/*---------------------------------
Exposing Meta Information in Rest API
----------------------------------*/

add_action( 'rest_api_init', 'create_api_posts_meta_field' );
 
function create_api_posts_meta_field() {
 
 // register_rest_field ( 'name-of-post-type', 'name-of-field-to-return', array-of-callbacks-and-schema() )
 register_rest_field( 'hc_email_signature', 'post-meta-fields', array(
 'get_callback' => 'get_post_meta_for_api',
 'schema' => null,
 )
 );
}
 
function get_post_meta_for_api( $object ) {
 //get the id of the post object array
 $post_id = $object['id'];
 
 //return the post meta
 return get_post_meta( $post_id );
}

