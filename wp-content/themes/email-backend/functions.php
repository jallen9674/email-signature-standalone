<?php
/******************************************************************
 * Square 4 Starter Theme
 * Built on Bones By Eddie Machado
 ******************************************************************/


/*--------------------------------------------
Brining in all of Bones, Start it Up!
---------------------------------------------*/

//Bones Core
require_once('assets/bones.php');

//Admin Modifications
require_once('assets/admin.php');

function bones_ahoy() {

	// launching operation cleanup
	add_action('init', 'bones_head_cleanup');

	// A better title
	add_filter('wp_title', 'rw_title', 10, 3);

	// remove WP version from RSS
	add_filter('the_generator', 'bones_rss_version');

	// launching this stuff after theme setup
	bones_theme_support();

} /* end bones ahoy */

// Get it Started! Miley style.
add_action('after_setup_theme', 'bones_ahoy');


/*--------------------------------------------
Thumbnail Sizes
---------------------------------------------*/


add_image_size('bones-thumb-600', 600, 150, true);
add_image_size('bones-thumb-300', 300, 100, true);
add_image_size('bones-thumb-page', 300, 150);
add_image_size('attorney-thumb', 300, 300);
add_image_size('faq-thumb', 300, 200, true);
add_image_size('faq-thumb-sidebar', 400, 250, true);
add_image_size('community-thumb', 200, 200, true);

add_filter('image_size_names_choose', 'square4_nice_image_sizes');

function square4_nice_image_sizes($sizes) {
	return array_merge($sizes, array(
		'bones-thumb-600'  => __('600px by 150px'),
		'bones-thumb-300'  => __('300px by 100px'),
		'bones-thumb-page' => __('300px by 150px'),

	));
}


/*--------------------------------------------
Bringing in CMB2
---------------------------------------------*/

require_once dirname(__FILE__) . '/assets/cmb2/init.php';

function update_cmb_meta_box_url($url) {
	$url = '/wp-content/themes/email-backend/assets/cmb2';
	return $url;
}

add_filter('cmb2_meta_box_url', 'update_cmb_meta_box_url');
?>