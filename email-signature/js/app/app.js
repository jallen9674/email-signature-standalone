
//Environment Variables
const WORDPRESS_ENDPOINT = 'https://email-signature.jsallen.com/wp-json/wp/v2/hc_email_signature?_embed&per_page=100';
const APP_PATH = 'https://email-signature.jsallen.com/email-signature/';

//Email Signature Template (This is Copied)
Vue.component('email-template', { 
  props: ['previewData', 'preview', 'path'],
  template: `<div :class="'preview-' + preview">
              <br class="hide-preview">
              <hr class="hide-preview" style="margin-top: 15px; text-align:left;">
        
              <table cellspacing="0" cellpadding="0" border="0" style="width: 550px; margin-top: 30px;">
                <tbody>
                  <tr>

                    <td style="vertical-align: middle;padding-right: 10px;">
                      <table cellspacing="0" cellpadding="0" border="0" style="padding-left: 10px; width: 100%;">
                        <tbody>

                          <tr>
                            <td v-if="previewData.photo" style="padding-right: 15px;">                            
                                <img id="TemplateLogo" class="preview-image" :src="previewData.photo" :alt="previewData.name" height="70" width="70" style="height: 70px; width: 70px; border-radius: 3px;">
                             
                            </td>
                          
                            <td v-if="previewData.photo" style="padding-left: 0px; font-family: Arial; font-size: 13pt; color: rgb(51, 51, 51); vertical-align: top; padding-top: 15px;" colspan="4"> 
                              <strong>{{ previewData.name }}</strong>
                              <br> 
                              <font style="font-family: Arial; font-size: 10pt;">{{previewData.title}}</font>
                            </td>
                            <td v-else style="padding-left: 0px;  padding-right: 15px; font-family: Arial; font-size: 13pt; color: rgb(51, 51, 51); vertical-align: top;" colspan="4"> 
                              <strong>{{ previewData.name }}</strong>
                              <br> 
                              <font style="font-family: Arial; font-size: 10pt;">{{previewData.title}}</font>
                            </td>
                          </tr>

                          <tr>
                            <td style="padding-right: 10px;  padding-top: 15px;font-family: Arial; font-size: 9pt; color: rgb(51, 51, 51); width: 75px;" colspan="1"> <strong>Email:</strong>
                            </td>
                            <td style="font-family: Arial; font-size: 9pt; padding-top: 15px;" colspan="3"> 
                              <font style="font-family: Arial; font-size: 9pt;">
                                  <a :href="'mailto:' + previewData.email" style="color: #0e4c26">{{previewData.email}}</a>
                              </font>
                            </td>
                          </tr>
                          
                          <tr>
                            <td style="padding-right: 10px; padding-top: 5px; font-family: Arial; font-size: 9pt; color: rgb(51, 51, 51); width: 75px;" colspan="1"> <strong>Website:</strong>
                            </td>
                            <td style="font-family: Arial; font-size: 9pt; padding-top: 5px;" colspan="3"> 
                              <font style="font-family: Arial; font-size: 9pt;">
                                  <a href="https://email-signature.jsallen.com" style="color: #0e4c26">www.website.com</a>
                              </font>
                            </td>
                          </tr>

                          <tr>
                            <td align="left" style="vertical-align: middle; padding-right: 10px; padding-top: 15px;" colspan="4">
                              <a target="_blank" href="#">
                                <img :src="'https://email-signature.jsallen.com/wp-content/uploads/2020/08/facebook-icon.png'">
                              </a>
                              <a target="_blank" href="#">
                                <img :src="'https://email-signature.jsallen.com/wp-content/uploads/2020/08/twitter-icon.png'">
                              </a>
                              <a target="_blank" href="#">
                                <img :src="'https://email-signature.jsallen.com/wp-content/uploads/2020/08/youtube-icon.png'">
                              </a>
                              <a target="_blank" href="#">
                                <img :src="'https://email-signature.jsallen.com/wp-content/uploads/2020/08/linkedin-icon.png'">
                              </a>
                            </td>
                          </tr>

                        </tbody>
                      </table>
                    </td>

                    <td align="left" style="vertical-align: middle; padding-left: 10px; border-left: 1px solid #414141;">
                      <table cellspacing="0" cellpadding="0" border="0" style="padding-left: 10px;">
                        <tbody>
                          
                          <tr>
                            <td align="left" style="vertical-align: middle; padding-right: 10px;" colspan="3">
                              <a href="https://email-signature.jsallen.com">
                                <img id="TemplateLogo" data-class="external" src="https://email-signature.jsallen.com/wp-content/uploads/2020/08/logo-crop.jpg" alt="Demo" style="display: block; max-width: 200px; border-radius: 3px;">
                              </a>
                            </td>
                          </tr>

                          <tr>
                            <td style="padding-right: 10px; padding-top: 15px; font-family: Arial; font-size: 10pt; color: rgb(51, 51, 51); display: inline-block; min-width: 75px;"> 
                              <strong>Toll Free:</strong>
                            </td>
                            <td style="font-family: Arial; font-size: 10pt; padding-top: 15px; display: inline-block;" colspan="2"> 
                              <font style="font-family: Arial; font-size: 10pt;">
                                  <a href="tel:800-999-2626" style="color: #0e4c26">800-999-2626</a>
                              </font>
                            </td>
                          </tr>

                          <tr>
                            <td style="padding-right: 10px; padding-top: 5px; font-family: Arial; font-size: 10pt; color: rgb(51, 51, 51); display: inline-block; min-width: 75px;"> 
                              <strong>Office:</strong>
                            </td>
                            <td style="font-family: Arial; font-size: 10pt; padding-top: 5px; display: inline-block;" colspan="2">  
                              <font style="font-family: Arial; font-size: 10pt;">
                                  <a :href="'tel:' + previewData.phone" style="color: #0e4c26">{{previewData.phone}}</a>
                              </font>
                            </td>
                          </tr>

                          <tr>
                            <td style="padding-right: 10px; padding-top: 5px; font-family: Arial; font-size: 10pt; color: rgb(51, 51, 51); display: inline-block; min-width: 75px;"> 
                              <strong>Fax:</strong>
                            </td>
                            <td style="font-family: Arial; font-size: 10pt; padding-top: 5px; display: inline-block;" colspan="2"> 
                              <font style="font-family: Arial; font-size: 10pt;">
                                  <a href="tel:412-661-9423" style="color: #0e4c26">412-661-9423</a>
                              </font>
                            </td>
                          </tr>

                        </tbody>
                      </table>
                    </td>

                  </tr>

                  <tr>
                    <td data-content="disclaimer" style="max-width: 500px; text-align: justify; padding-top: 15px; font-size: 8pt; color: rgb(136, 136, 136);" colspan="2"> 
                      <font style="font-family: Arial; font-size: 8pt; color: rgb(136, 136, 136);">
                        The above information may be confidential or subject to attorney-client privilege or the attorney work-product privilege.  If you have received this message in error, or receive an unintended attachment, please notify the sender immediately and delete the message or attachment from your email without replicating the message or attachment.  Any hard copies should be destroyed.  Any unauthorized use or distribution of this email or attachments is prohibited.
                      </font>
                    </td>
                  </tr>

                </tbody>
              </table>
            </div>`
});


var app = new Vue({   
    el: '#app',
    data: {
      "APP_PATH": APP_PATH,
      "id": '',
      "preview": {
        "id": '',
        "name": '',
        "email": '',
        "title": '',
        "photo": '',
        "phone": ''
      },      
      "apiInfo": null
    },
    mounted () {
     axios
        .get(WORDPRESS_ENDPOINT)
        .then(response => (
            this.apiInfo = response.data
          ))
        .catch(function (error) {
          console.log(error);
          alertify.error('Error Fetching WordPress Data');

          //Trigger Update (To Make Demo Data Searchable)
          app.$forceUpdate();
        });
    },
    updated(){
      var teamList = new List('team-list', {valueNames: [ 'name' ]});
    },
    methods: {
      
      previewEmailSignature(id){
        let currentEmail = id;

        //Loop Over Dataset and Get Data if Matches
        for(let i = 0; i < this.apiInfo.length; i++) {
          
          if(currentEmail == this.apiInfo[i].id){

            //Reset Values
            this.preview.id = undefined;
            this.preview.name = undefined;
            this.preview.email = undefined;
            this.preview.title = undefined;
            this.preview.phone = undefined;
            this.preview.photo = undefined;

            //Set Preview Values
            this.preview.id = this.apiInfo[i].id
            this.preview.name = this.apiInfo[i]['post-meta-fields']['_hc_email_name'][0]
            this.preview.email = this.apiInfo[i]['post-meta-fields']['_hc_email_email'][0]
            this.preview.title = this.apiInfo[i]['post-meta-fields']['_hc_email_title'][0]
            this.preview.phone = this.apiInfo[i]['post-meta-fields']['_hc_email_phone'][0]
            this.preview.photo = this.apiInfo[i]['_embedded']['wp:featuredmedia']['0']['media_details']['sizes']['hc-email-signature-thumbnail'].source_url;
            
            alertify.success(this.preview.name + '\'s Signature Selected!');
            
          } 
        }

      },
      copyHTMLSignature(){

        if (this.preview.name){
          var copyPreviewArea = document.querySelector('.preview-true');
          clipboard.writeText(copyPreviewArea.innerHTML);
          console.log('HTML Preview copied: ' + this.preview.name);
          alertify.success('Copied HTML Signature for ' + this.preview.name);
        } else {
          //console.log('Please select a signature.');
          alertify.error('Error :(');
        }

      },
      copyRichSignature() {

        if (this.preview.name) {
          var copyPreviewArea = document.querySelector('.preview-true');
          var dt = new clipboard.DT();
            dt.setData("text/plain", copyPreviewArea.innerHTML);
            dt.setData("text/html", copyPreviewArea.innerHTML);
            clipboard.write(dt);     
          //console.log('Copied Rich Text Signature for ' + this.preview.name);
          alertify.success('Copied Rich Text Signature for ' + this.preview.name);
        } else {
          console.log('Please select a signature.');
          alertify.error('Error :(');
        }

      }     
    }
  });